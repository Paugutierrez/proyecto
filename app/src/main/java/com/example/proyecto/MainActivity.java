package com.example.proyecto;

import androidx.appcompat.app.AppCompatActivity;

import android.database.sqlite.SQLiteDatabase;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

import database.AgendaDbHelper;

public class MainActivity extends AppCompatActivity {

    private TextView lblrespuesta;
    private Button btnprobar;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        lblrespuesta = (TextView) findViewById(R.id.lblrespuesta);
        btnprobar =  (Button) findViewById(R.id.bntprobar);

        btnprobar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                String dataBaseName="";
                SQLiteDatabase db;
                AgendaDbHelper helper = new AgendaDbHelper(MainActivity.this);
                db= helper.getWritableDatabase();
                helper.onUpgrade(db,1,2);
                dataBaseName = helper.getDatabaseName();

                lblrespuesta.setText(dataBaseName);


            }
        });
    }
}
